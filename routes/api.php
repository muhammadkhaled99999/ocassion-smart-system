<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\ProductController;
use Illuminate\Auth\Events\Login;


Route::resource('product' , 'ProductController');
Route::post('login' , 'passportController@login');
Route::post('register' , 'passportController@register');
Route::get("/product/{id}","productcontroller@show");
Route::get("user","passportController@details");


//Route::middleware('auth:api')->get('/user', function (Request $request) {


//});
