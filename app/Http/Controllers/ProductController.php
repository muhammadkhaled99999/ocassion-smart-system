<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function index()
    {
        $product = Product::all();
       return response()->json([
        'sucess' => true,
        'data' => $product,

       ]);
    }


    public function store(Request $request)
    {
    $this->validate($request, [
        'name' => 'required',
        'price' => 'required|integer',
        'location' => 'required',
        'Rate' => 'required',
        'images' => 'required',
        'Catgoery' => 'required',
    ]);

      $product = new Product();
      $product->name = $request->name;
      $product->price =$request->price;
      $product->location =$request->location;
      $product->Rate =$request->Rate;
      $product->images =$request->images;
      $product->Catgoery =$request->Catgoery;
      $product->save();


      return response()->json([
        'success' => true,

    ] , 200);

    }


    public function show($id)
    {

        $product= Product::all()->where('Catgoery',$id);
        if(!$product){
            return response()->json([
               'success' => false,
               'data' => 'product with id' . $id . 'not found',
            ], 400);
        };

        return response()->json([
            'success' => true,
            'data' => $product->toArray(),
        ], 400);
    }



    public function update(Request $request,$id)
    {
        $product = auth()->user()->products->find($id);
        if (!$product){
          return response()->json([
            'success' => false,
             'data' => 'product with id' . $id . 'not found',
          ] , 500);
        }
      $updated = $product->fill($request->all())->save();

      if($updated){
          return response()->json([
              'success' => true,
              'data' => 'product updated successfully',
          ] , 200);
      }else {
          return response()->json([
              'success' => false,
              'data' => 'product could not be updated',
          ] , 500);
      }
    }


    public function destroy(Product $product , $id)
    {
        $product = auth()->user()->products->find($id);
        if(!$product){
            return response()->json([
                'success' => false,
                'message' => 'product with id' . $id . 'not found',
            ],400);
        }

        if ($product->delete()){
            return response()->json([
                'success' => true,
              'message' => 'product deleted successfully',
            ]);
        }else {
            return response()->json([
                'success' => false,
                'message' => 'product could not be deleted',
            ], 500);
        }
    }
}
